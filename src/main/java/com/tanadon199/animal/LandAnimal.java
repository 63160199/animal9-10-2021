/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tanadon199.animal;

/**
 *
 * @author Kitty
 */
public  abstract class LandAnimal   extends Animal {
    public LandAnimal(String name,int numofleg){
        super(name,numofleg);
    }
    public abstract void run();
    
}
