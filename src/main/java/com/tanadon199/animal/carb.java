/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tanadon199.animal;

/**
 *
 * @author Kitty
 */
public class carb extends AquiaticAnimal {
    private String nickname;
    public carb(String nickname) {
        super("carb");
        this.nickname = nickname;
    }

    public void run() {
        System.out.println("carb: "+ nickname + "run");
    }

    @Override
    public void eat() {
         System.out.println("carb: "+ nickname + "eat");
    }

    @Override
    public void walk() {
          System.out.println("carb: "+ nickname + "walk");
    }

    @Override
    public void speak() {
          System.out.println("carb: "+ nickname + "speak");
    }

    @Override
    public void sleep() {
         System.out.println("carb: "+ nickname + "sleep");
    }

    @Override
    public void swim() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
